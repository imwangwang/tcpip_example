#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "tcp.h"
#include "thread.h"


void * handle_conn(void *vptr) {
    THREAD_PARAMS *params = (THREAD_PARAMS *)vptr;

    struct sockaddr_storage *pca;
    socklen_t addrlen;
    int csock;
    HEADER packet_hdr;
    unsigned len = 0;

    do {
        if (params == NULL) {
            break;
        }
        
        pca = (struct sockaddr_storage *)&(params->ca);
        addrlen = sizeof(struct sockaddr_storage);
        csock = accept(params->ssock, (struct sockaddr *)pca, &addrlen);

        if (csock == -1) {
            perror("accept failure");
            break;
        }
        else {
            char* ip;
            
            ip = get_ip_addr(pca);
            if (ip) {
                printf("server: got connection from %s\n", ip);
                free(ip);
            }
            else {
                printf("server: failed to get ip address of the client!");
            }

            /* gonna get something from client side */

            do {
                len = recv(csock, (unsigned char *)&packet_hdr, sizeof(packet_hdr), 0);
                if (len != sizeof(packet_hdr)) {
                    perror("recv unexpected data");
                    break;
                }

                if (packet_hdr.header_magic == 0xA5) {
                    printf("server: got packet header, cmd:0x%02X, len:0x%04X\n", 
                            packet_hdr.command, packet_hdr.length);
                    send(csock, "OK", 2, 0);
                }
                else {
                    send(csock, "FL", 2, 0);
                    break;
                }
 
            } while (1);
        } 
        
    } while (0);

    if (csock != -1) {
        close(csock);
    }

    free(params);
    
    pthread_exit(0);
}

