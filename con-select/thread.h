#ifndef __THREAD_H__
#define __THREAD_H__


#include <pthread.h>
#include "tcp.h"

typedef struct __header {
    unsigned char header_magic;
    unsigned char command;
    unsigned short length;
} HEADER;

typedef struct __thread_params {
    int ssock;
    struct sockaddr_storage ca;
} THREAD_PARAMS;

#define MAX_THREADS     16

void * handle_conn(void *vptr);


#endif

